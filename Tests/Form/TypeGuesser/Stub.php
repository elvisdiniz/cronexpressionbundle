<?php

declare(strict_types=1);

namespace Setono\CronExpressionBundle\Tests\Form\TypeGuesser;

final class Stub
{
    /**
     * @var \Cron\CronExpression
     */
    private $property;
}
